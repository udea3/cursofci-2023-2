from ising import Ising

if __name__ == '__main__':
    J = 2
    T = 100
    beta = 100
    N = 100
    primero = Ising(J, T, beta, N)

    res = primero.MC()
    primero.figMagnetizacion(res)
    primero.figCv(res)