from isingMC import Modelo_Ising


if __name__ == "__main__":
    
    #Definimos los parámetros de la simulación 
    
    L = [2,4,6,8,10]         # Tamaño de la red (LxL), debe contener al menos un elemento 
    iterations = 2000   # Número de iteraciones
    
    #Creamos una instancia de la clase IsingMC
    
    ising = Modelo_Ising(L,iterations)
    ising.run()