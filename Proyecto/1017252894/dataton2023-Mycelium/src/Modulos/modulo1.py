#Importar modulos necesarios
import numba as nb
import random

@nb.jit(nopython=True)
def PausaActia(jornada,hora_inicio,hora_fin):
    """
    Esta función toma un rango de la jornada de trabajo y añade 
    las pausas activas necesarias hasta que se cumplan las condiciones
    requeridas
    """
    while ( abs(hora_inicio-hora_fin) > 8):
        temp = random.randint(hora_inicio+4,hora_inicio+8)
        if ( abs(temp - hora_fin) > 4):
            jornada[temp] = 2
            hora_inicio = temp+1

@nb.jit(nopython=True)
def CalcularJornada(jornada,dia,ultima_hora_semana,ultima_hora_sabado):
    """
    Esta función retorna una array con la jornada de un solo empleado de un solo dia
    0 == Nada
    1 == trabaja
    2 == PausaActiva
    3 == Almuerza
    """
    # jornada[0] = Documento
    # jornada[1] = Tipo de contrato 8==Tc 4==MT
    # jornada[2] = Quienes son los que entran a primera y ultima hora
    # jornada[3] = Hora inicio de jornada
    # jornada[4] = Hora almuerzo de TC

    # Tiempo Completo -------------------------------------------------------------------------------
    if int(jornada[1]) == 8:
        # Jornada para los TC
        if jornada[3] == -1:
            # Si esta en -1 es porque aun no se ha definido una hora de entrada 
            if (jornada[2] == 1):
                # El primer empleado empieza a primera hora
                jornada[3] = 0
            elif (jornada[2] == 2):
                # El segundo empleado sale a ultima hora 
                jornada[3] = ultima_hora_semana-34
            else:
                # los demas tiene una hora de inicio aleatoria 
                jornada[3] = random.randint(0,ultima_hora_semana-34)

        hora_inicio = int(jornada[3])

        # Fijar la hora de salida en función de la hora de inicio
        hora_fin = hora_inicio+34

        if jornada[4] == 0:
            # Si esta en 0 es porque aun no se ha definido una hora de almuerzo 
            jornada[4] = random.randint(16,24)

        almuerzo = int(jornada[4])

        # Inicializar la jornada en ceros
        jornada[dia*49+5:(dia+1)*49+5] = 0

        if dia<5:
            # Si el dia es menor que 5, es porque se trata de un dia entre semana lunes-viernes
            jornada[hora_inicio +dia*49+5:hora_fin +dia*49+5] = 1
            jornada[almuerzo  +dia*49+5:almuerzo+6  +dia*49+5] = 3

            PausaActia(jornada,hora_inicio +dia*49+5,almuerzo  +dia*49+5)
            PausaActia(jornada,almuerzo+6  +dia*49+5,hora_fin +dia*49+5)
            #jornada[almuerzo+dia_*49+3:almuerzo+dia_*49+3+6] = 3

        elif dia==5:
            if ultima_hora_sabado-20 < 0:
                hora_inicio = 0
            else:
                hora_inicio = random.randint(0,ultima_hora_sabado-20)
            
            hora_fin = hora_inicio + 20
            
            jornada[hora_inicio +dia*49+5:hora_fin +dia*49+5] = 1
            PausaActia(jornada,hora_inicio +dia*49+5,hora_fin +dia*49+5)

    # Medio Tiempo -------------------------------------------------------------------------------
    elif int(jornada[1]) == 4:
        if jornada[3] == -1:
            if jornada[2] == 1:
                jornada[3] = 0
            
            elif jornada[2] == 2:
                jornada[3] = ultima_hora_semana-16

            else:   
                jornada[3] = random.randint(0,ultima_hora_semana-16)
            # Elegir la hora de entrada
        
            if ultima_hora_semana-16 < 0:
                jornada[3] = 0

        if dia<5:
            hora_inicio = int(jornada[3])
        elif dia==5:
            if ultima_hora_sabado-16 < 0:
                hora_inicio = 0
            else:
                hora_inicio = random.randint(0,ultima_hora_sabado-16)
            
        hora_fin = hora_inicio+16

        # Inicializar la jornada en ceros
        jornada[dia*49+5:(dia+1)*49+5] = 0

        jornada[hora_inicio +dia*49+5:hora_fin +dia*49+5] = 1
        PausaActia(jornada,hora_inicio +dia*49+5,hora_fin +dia*49+5)

@nb.jit(nopython=True)
def CalcularDemanda(horario):
    """
    Esta función calcula la diferencia entre empleados disponibles
    y la cantidad de empleados necesarios para cada franja horaria
    """
    empleados = len(horario[0])-3
    for i in range(len(horario[:,0])):
        total_trabaja = 0
        for j in range(empleados):
            if (horario[i][j] == 1.0):
                total_trabaja += 1
        
        horario[i][empleados] = total_trabaja

    horario[:,empleados+2] = -1*(horario[:,empleados] - horario[:,empleados+1] )
    #Total                  # Disponibilidad       # Demanda

# ----------------------------------------------------------------------------------------------------
@nb.jit(nopython=True)
def CalcularPuntaje(horario):
    """
    Esta función retorna el puntaje del horario
    """
    empleados = len(horario[0])-3
    puntaje = 0
    for i in range(5,len(horario[:,0])):
        disponible = horario[:,empleados+2][i]

        if (disponible > 0):
            puntaje += disponible
    
    return puntaje

# ----------------------------------------------------------------------------------------------------
@nb.jit(nopython=True)
def Correcto(horario):
    """
    Esta función determina si para cada franja horaria hay al menos
    un empleado disponible
    """
    empleados = len(horario[0])-3
    disponible = True
    for i in range(5,len(horario[:,0])):
        if horario[:,empleados][i] == 0 and horario[:,empleados+1][i] != 0: 
            disponible = False
    return disponible