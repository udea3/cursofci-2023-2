# Partícula en un Campo Magnético

Este es un proyecto que simula el movimiento de una partícula cargada en un campo magnético. El proyecto está implementado en Python y utiliza la biblioteca NumPy y Scipy para cálculos numéricos y Matplotlib para la visualización.

## Descripción

La clase `ParticulalEnCampoMagnetico` modela el movimiento de una partícula cargada en una región con campo magnético. La partícula inicia su movimiento en una trayectoria libre y luego entra en una región con campo magnético, la cual se encuentra en la parte positiva del eje Y.Aquí, su movimiento se ve significativamente influenciado por la interacción con este campo magnético.

### Fase de Trayectoria Libre

En la primera fase, la partícula sigue una trayectoria libre antes de ingresar a la región con el campo magnético. Esta trayectoria inicial se calcula a partir de las condiciones iniciales proporcionadas, que incluyen la velocidad inicial y la posición inicial.

### Fase dentro del Campo Magnético

Una vez que la partícula ingresa en la región con el campo magnético, su movimiento se rige por las ecuaciones de movimiento en presencia de dicho campo. Durante esta fase, la partícula experimenta desviaciones y cambios significativos en su trayectoria debido a la interacción con el campo magnético.

### Ecuaciones de Movimiento

Las ecuaciones de movimiento para la partícula cargada se derivan de la ecuación de Lorentz, que describe la interacción entre una partícula cargada, su velocidad y un campo magnético:

$$ \vec{F} = q \vec{v} \times \vec{B} $$

Donde:
- $\vec{F}$ es la fuerza experimentada por la partícula.
- $q$ es la carga de la partícula.
- $\vec{v}$ es la velocidad de la partícula.
- $\vec{B}$ es el campo magnético.

Esto conduce a las siguientes ecuaciones de movimiento para la partícula cargada:

$$
\begin{align*}
\ddot{x} &= \frac{qB}{m} \dot{y} \\
\ddot{y} &= -\frac{qB}{m} \dot{x} \\
\ddot{z} &= 0
\end{align*}
$$

Estas ecuaciones describen cómo las componentes de la velocidad de la partícula cambian en respuesta al campo magnético.


### Condiciones Iniciales

Las condiciones iniciales se definen como sigue:

- **Velocidad Inicial ($\vec{v}(0)$):** Calculada a partir de la energía cinética $Ek$; $v = \sqrt{\frac{2Ek}{m}}$.

- **Ángulos $\theta$ y $\varphi$:** El ángulo $\theta$ es el angulo entre el campo magnetico y la velocidad de la particuya y $\varphi$ es el angulo que forma la proyeccion de la velocidad sobre el plano XY y el eje X

- **Posición Inicial $\vec{r}(0)$:** Dada como un vector tridimensional $(x(0), y(0), z(0))$, indicando la ubicación inicial de la partícula en el espacio 3D.


### Resolución Numérica

La simulación de la partícula en movimiento en el campo magnético se logra mediante la resolución numérica de las ecuaciones de movimiento. Para esta tarea, se utiliza el método `odeint` de la biblioteca Scipy.

### Funcionalidades del Proyecto

El proyecto ofrece una serie de funcionalidades que permiten explorar el comportamiento de la partícula en el campo magnético:

- **Antes de Ingresar al Campo Magnético:** La partícula sigue una trayectoria libre inicial calculada a partir de sus condiciones iniciales.

- **Punto de Entrada al Campo Magnético:** Se identifica el punto en el que la partícula ingresa a la región con el campo magnético.

- **Ecuación de Movimiento:** Se establece la ecuación de movimiento de la partícula cargada en el campo magnético, como se describe anteriormente.

- **Trayectoria en el Campo Magnético:** Se calcula y registra la trayectoria completa de la partícula en la presencia del campo magnético.

- **Filtrado de Trayectoria:** Se ofrece la opción de filtrar la trayectoria para visualizar únicamente la parte en la que la partícula se encuentra dentro de la región del campo magnético.

- **Gráfica de la Trayectoria:** Se proporciona una visualización tridimensional de la trayectoria de la partícula, que incluye tanto el movimiento libre inicial como el recorrido en el campo magnético.


## Uso

Para utilizar este proyecto, sigue estos pasos:

1. Instala las dependencias del proyecto desde el archivo `requirements.txt`:
```bash
pip install -r requirements.txt 
```
2. Importa la clase `ParticulalEnCampoMagnetico` en tu código y crea una instancia con las condiciones iniciales de tu partícula.

3. Utiliza los métodos de la clase para simular y visualizar el movimiento de la partícula.

4. Puedes ajustar los parámetros del campo magnético, condiciones iniciales y otros valores según tus necesidades.


## Advertencia

A la hora de cambiar los parámetros iniciales en la mayoría de las ocasiones obtendrá `Error en los parámetros` debido a la necesidad de mantener coherencia entre las condiciones iniciales. Ademas, el t_final debe ser escalado según los valores de los demás parámetros, tarea que no resulta ser sencilla en la mayoría de los casos. Por tanto, a la hora de modificar los parámetros se debe tener paciencia hasta conseguir unos resultados satisfactorios.

## Autor

- [M. Luciano]

