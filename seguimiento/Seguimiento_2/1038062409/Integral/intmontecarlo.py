import scipy.integrate as integrate
import matplotlib.pyplot as plt
import numpy as np


class intMC(object):
    """
    Esta clase resuelve integrales con el metodo de Monte Carlo y compara con el valor exacto
    tambien grafica el valor de la integral en funcion del numero de iteraciones
    
    Parametros:
    f: funcion a integrar
    a: limite inferior de la integral
    b: limite superior de la integral
    N: numero de puntos aleatorios
    
    Salida:
    SolucionMontecarlo: solucion numerica de la integral
    SolucionExacta: solucion exacta de la integral
    Grafica: grafica del valor de la integral en funcion del numero de iteraciones
    """
    
    def __init__(self, f, a, b, N):
        """
        Inicializa la clase con los parametros de la integral
        """
        self.f = f
        self.a = a
        self.b = b
        self.N = N
    
    def continuous(self):
        x = np.linspace(self.a, self.b, self.N)
        is_continuous  = all(abs(self.f(x) - self.f(x + 1e-9)) < 1e-6 for x in x[:-1]) 
        #is_continuous = np.all(np.isfinite(self.f(x)))
        return is_continuous
    
    def integrateMC(self):
        """
        Esta funcion realiza la integral por el metodo de Monte Carlo
        """
        # Genera N numeros aleatorios entre a y b
        x = np.random.uniform(self.a, self.b, self.N)
        
        # Evalua la funcion en los puntos aleatorios
        fx = self.f(x)
        
        # Calcula la integral
        I = ((self.b - self.a))* np.mean(fx) 
        return I
    
    def integrateEx(self):
        """
        Esta funcion calcula la integral exacta
        """
        sol , _ = integrate.quad(self.f, self.a, self.b)
        return sol
        
    def plotMC(self):
        """
        Esta funcion grafica el valor de la integral en funcion del numero de iteraciones
        """
        #Iteraciones
        x = [i for i in range(1,self.N,10)]
        
        #Valor numerico de la integral para cada iteracion
        valor_numerico = [(self.b - self.a) * np.mean(self.f(np.random.uniform(self.a, self.b, i))) \
                          for i in range(1,self.N,10)]
        
        #Valor exacto de la integral para cada iteracion
        valor_exacto = [self.integrateEx() for i in range(1,self.N,10)]
        
        #Grafica
        _ , ax = plt.subplots()
        ax.plot(x,valor_numerico,"k-",label="Valor de la integral con Monte Carlo") 
        #con linea mas delgada
        ax.plot(x,valor_exacto,"r-",label = "Valor analitico de la integral", linewidth=1)
        ax.set_xlabel("Iteraciones")
        ax.set_ylabel("Valor de la integral")
        ax.legend()
        ax.grid()        
        #guarda la grafica
        plt.savefig("intmontecarlo.png")
        plt.show()

    def run(self):
        """
        Esta funcion ejecuta todos los metodos de la clase intMC para hacer manejo de errores
        """
        if self.a > self.b or self.a == self.b:
            raise ValueError("El limite inferior debe ser menor que el limite superior")
        if self.N <= 0:
            raise ValueError("El numero de puntos debe ser mayor que cero")
        
        if not self.continuous():
            raise ValueError("La funcion debe ser continua en el intervalo [a,b]")
        try:
            SolucionMontecarlo = self.integrateMC()
            SolucionExacta = self.integrateEx()
            Grafica = self.plotMC()
            return Grafica
        except Exception as e:
            raise ValueError("Cambie los parametros de la integral o la funcion a integrar")